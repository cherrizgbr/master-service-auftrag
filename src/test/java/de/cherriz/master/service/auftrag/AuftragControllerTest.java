package de.cherriz.master.service.auftrag;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Testklasse für den AuftragsService.
 *
 * @author Frederik Kirsch
 */
@RunWith(MockitoJUnitRunner.class)
public class AuftragControllerTest {

    @Mock
    private AuftragService auftragService;

    private AuftragController auftragController;

    /**
     * Initialisiert den AuftragController.
     */
    @Before
    public void setUp() {
        this.auftragController = new AuftragController(auftragService);
    }

    /**
     * Testet die Erzeugung eines Auftrags.
     */
    @Test
    public void createAuftrag() {
        //Vorbereitung
        Auftrag auftrag = this.getTestAuftrag("test", 1);

        //Aufruf
        Long id = this.auftragController.create(auftrag);

        //Validierung
        assertNotNull(id);
    }

    /**
     * Testet das Lesen eines Auftrags.
     */
    @Test
    public void readAuftrag() {
        //Vorbereitung
        String prozess = "test";
        int anz = 2;
        Auftrag auftrag = this.getTestAuftrag(prozess, anz);
        Long id = this.auftragController.create(auftrag);

        //Aufruf
        Auftrag result = this.auftragController.get(id);

        //Validierung
        assertNotNull(auftrag);
        assertEquals(auftrag.getProzess(), prozess);
        assertEquals(auftrag.getAuftragsDaten().size(), 2);
    }

    /**
     * Hilfsklasse dient der Erzeugung eines Testauftrags.
     *
     * @param prozess  Der Prozess des Auftrags.
     * @param anzDaten Die Anzahl der Attribute des Auftrags.
     * @return Der generierte Testauftrag.
     */
    private Auftrag getTestAuftrag(String prozess, int anzDaten) {
        String attributeA = "attribut";
        String valueA = "value";

        Auftrag auftrag = new Auftrag(prozess);
        for (int i = 1; i <= anzDaten; i++) {
            auftrag.addAuftragsDatum(new AuftragsDatum(attributeA + " " + i, valueA + " " + i));
        }

        return auftrag;
    }

}