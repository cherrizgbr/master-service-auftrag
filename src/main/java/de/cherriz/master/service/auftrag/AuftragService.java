package de.cherriz.master.service.auftrag;

/**
 * Schnittstelle des AuftragsService.
 *
 * @author Frederik Kirsch
 */
public interface AuftragService {

    /**
     * Speichert einen Auftrag und gibt die ID des Objekts zurueck.
     *
     * @param auftrag Der zu speichernde Auftrag.
     * @return Die ID.
     */
    Long create(Auftrag auftrag);

    /**
     * Liefert den Auftrag zu einer ID.
     *
     * @param id Die ID.
     * @return Der Auftrag.
     */
    Auftrag find(Long id);

}