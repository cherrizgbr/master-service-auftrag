package de.cherriz.master.service.auftrag;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.inject.Inject;

/**
 * Die Implementierung des {@link de.cherriz.master.service.auftrag.AuftragService}.
 *
 * @author Frederik Kirsch
 */
@Service
@Validated
public class AuftragServiceImpl implements AuftragService {

    private final AuftragRepository repository;

    /**
     * Setzt das Repository fuer den Datenbankzugriff.
     *
     * @param repository Das Repository.
     */
    @Inject
    public AuftragServiceImpl(final AuftragRepository repository) {
        this.repository = repository;
    }

    @Override
    public Long create(Auftrag auftrag) {
        this.repository.save(auftrag);
        return auftrag.getId();
    }

    @Override
    public Auftrag find(Long id) {
        return this.repository.findOne(id);
    }

}