package de.cherriz.master.service.auftrag;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity für einen einzelnen Wertes eines Auftrags.
 *
 * @author Frederik Kirsch
 */
@Entity
@XmlRootElement
public class AuftragsDatum {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long auftragID;

    private String attribute;

    private String value;

    /**
     * Leerer Konstruktor für generische Objekterzeugung.
     */
    public AuftragsDatum() {}

    /**
     * Konstruktor.
     *
     * @param attribute Der Attributname.
     * @param value Der Attributwert.
     */
    public AuftragsDatum(String attribute, String value) {
        this.setAttribute(attribute);
        this.setValue(value);
    }

    /**
     * @return Die ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * Setzt die ID.
     * @param id Die ID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return Den Attributnamen.
     */
    public String getAttribute() {
        return attribute;
    }

    /**
     * @return Die ID des zugehoerigen Auftrags.
     */
    public Long getAuftragID() {
        return auftragID;
    }

    /**
     * Setzt die ID des zugehoerigen Auftrags.
     *
     * @param auftragID Die AuftragsID.
     */
    public void setAuftragID(Long auftragID) {
        this.auftragID = auftragID;
    }

    /**
     * Setzt das Attribut.
     *
     * @param attribute Das Attribut.
     */
    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    /**
     * @return Der Wert.
     */
    public String getValue() {
        return value;
    }

    /**
     * Setzt den Wert.
     *
     * @param value Der Wert.
     */
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(AuftragsDatum.class.getName());
        builder.append(" {\n\t id: ");
        builder.append(this.getId());
        builder.append("\n\tauftragID: ");
        builder.append(this.getAuftragID());
        builder.append("\n\tattribute: ");
        builder.append(this.getAttribute());
        builder.append("\n\tvalue: ");
        builder.append(this.getValue());
        builder.append("\n}");
        return builder.toString();
    }

}