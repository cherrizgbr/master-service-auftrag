package de.cherriz.master.service.auftrag;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Auftrag.
 *
 * @author Frederik Kirsch
 */
@Entity
@XmlRootElement
public class Auftrag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private String prozess;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "auftragID", referencedColumnName = "id")
    private List<AuftragsDatum> auftragsDaten;

    /**
     * Leerer Konstruktor fuer generische Objekterzeugung.
     */
    public Auftrag() {

    }

    /**
     * Konstruktor.
     *
     * @param prozess Der Name des Prozesses des Auftrags.
     */
    public Auftrag(String prozess) {
        this.setProzess(prozess);
    }

    /**
     * @return Die AuftragsID.
     */
    public Long getId() {
        return id;
    }

    /**
     * Setzt die ID des Auftrags.
     *
     * @param id Die AuftragsID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return Der Prozess des Auftrags.
     */
    public String getProzess() {
        return prozess;
    }

    /**
     * Setzt den Prozess des Auftrags.
     *
     * @param prozess Der Name des Prozesses des Auftrags.
     */
    public void setProzess(String prozess) {
        this.prozess = prozess;
    }

    /**
     * @return Die Daten des Auftrags.
     */
    public List<AuftragsDatum> getAuftragsDaten() {
        return auftragsDaten;
    }

    /**
     * Setzt die Daten des Auftrags.
     *
     * @param auftragsDaten Die Datend es Auftrags.
     */
    public void setAuftragsDaten(List<AuftragsDatum> auftragsDaten) {
        this.auftragsDaten = auftragsDaten;
    }

    /**
     * Fuegt einen Einzelnen Datensatz zum Auftrag hinzu.
     *
     * @param datum Eine inzelner Datensatz.
     */
    public void addAuftragsDatum(AuftragsDatum datum) {
        if (this.getAuftragsDaten() == null) {
            this.setAuftragsDaten(new ArrayList<AuftragsDatum>());
        }
        this.auftragsDaten.add(datum);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(Auftrag.class.getName());
        builder.append(" {\n\t id: ");
        builder.append(this.getId());
        builder.append("\n\tprozess: ");
        builder.append(this.getProzess());
        builder.append("\n\tauftragsdaten: {");
        for (AuftragsDatum datum : this.getAuftragsDaten()) {
            builder.append("\n\t");
            builder.append(datum.toString());
        }
        builder.append("\n\t}");
        builder.append("\n}");
        return builder.toString();
    }

}
