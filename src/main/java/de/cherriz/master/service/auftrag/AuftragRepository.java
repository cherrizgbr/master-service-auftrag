package de.cherriz.master.service.auftrag;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Schnittstelle zum Repository fuer Auftraege.
 *
 * @author Frederik Kirsch
 */
public interface AuftragRepository extends JpaRepository<Auftrag, Long> {}