package de.cherriz.master.service.auftrag;

import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Kontroller für den REST Service zu Auftraegen.
 *
 * @author Frederik Kirsch
 */
@RestController
@RequestMapping(value = "/auftrag")
public class AuftragController {

    private final AuftragService auftragService;

    /**
     * Setzt den Service für die Verarbeitung.
     *
     * @param auftragService Der AuftragService.
     */
    @Inject
    public AuftragController(final AuftragService auftragService) {
        this.auftragService = auftragService;
    }

    /**
     * Liefert einen Auftrag zu einer AuftragsID.
     *
     * @param id Eine AuftragsID.
     * @return Der Auftrag.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Auftrag get(@PathVariable Long id) {
        Auftrag auftrag = this.auftragService.find(id);
        return auftrag;
    }

    /**
     * Speichert den uebergebenen Auftrag.
     * Reichert ihn um die ID an und gibt ihn zurueck.
     *
     * @param auftrag Der zu speichernde Auftrag.
     * @return Der gespeicherte Auftrag.
     */
    @RequestMapping(value = "/create", method = RequestMethod.PUT)
    public Long create(@RequestBody @Valid Auftrag auftrag) {
        Long auftragsID = this.auftragService.create(auftrag);
        return auftragsID;
    }

    /**
     * Methode dient zur Pruefung der Verfuegbarkeit des Services.
     *
     * @return den Status des Services.
     */
    @RequestMapping(value = "/systemcheck", method = RequestMethod.GET)
    public String hello() {
        String result = "SYSTEMCHECK:";
        result += "<br />EntityManager: " + (this.auftragService != null ? "OK" : "Error");
        return result;
    }

}